#!/bin/bash
set -e
. env.sh

cd ncurses
./configure --host=arm-linux-gnueabi

make -j`nproc`

#things get shady here
set +e

export CFLAGS+=-I$PWD/include
export LDFLAGS+=-L$PWD/lib

cd ../vim/
 
./configure --host=arm-linux-gnueabi CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS -static"
make -j`nproc`

cp src/vim ../vim.exe
