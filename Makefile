.PHONY: all clean reset

bin=vim.exe

all: $(bin)

$(bin):
	./compile.sh

clean:
	rm -f $(bin)
reset:
	rm -rf vim/ ncurses/
	git submodule update --init

