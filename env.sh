#!/bin/bash

export CROSS=arm-linux-gnueabi
export CC=$CROSS-gcc
export CXX=$CROSS-g++
export LD=$CROSS-ld
export AR=$CROSS-ar


export vim_cv_getcwd_broken=no
export vim_cv_memmove_handles_overlap=yes
export vim_cv_stat_ignores_slash=no
export vim_cv_terminfo=yes
export vim_cv_toupper_broken=no
export vim_cv_tty_group=world
export vim_cv_tgent=zero
